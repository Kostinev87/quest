<?php
/**
 * Created by PhpStorm.
 * User: Evgenii Kostin
 * Date: 16.02.16
 * Time: 22:37
 */

include_once __DIR__ . '/../vendor/autoload.php';

spl_autoload_register(function ($class) {
    include __DIR__ . '/../src/classes/'.$class.'.php';
});

$app = new App($argc, $argv);
