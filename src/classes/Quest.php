<?php
/**
 * Created by PhpStorm.
 * User: Evgenii Kostin
 * Date: 16.02.16
 * Time: 22:43
 */


use SpaceWeb\Quest\QuestAbstract;

/**
 * Class Quest
 *
 * Утилита для получения статистики
 */
class Quest extends QuestAbstract {

    private static  $instance;

    /**
     * Получает экземпляр самого себя
     *
     * @return Quest
     */
    public static function instance() {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Исполняемый метод получения статистики с заданными параметрами
     *
     * @throws BadCommandException
     */
    public function actionStatistic()
    {
        $args = func_get_args();

        $withOutDocumentsCondition = $args[0] && in_array('--without-documents', $args[0]);
        $withDocumentsCondition = $args[0] && in_array('--with-documents', $args[0]);

        if (!$withOutDocumentsCondition && !$withDocumentsCondition)
        {
            throw new BadCommandException('use args: --without-documents --with-documents');
        }

        echo 'Please enter start date:';
        $dateStart = fgets(STDIN) . ' 00:00:00';

        echo 'Please enter end date:';
        $dateEnd   = fgets(STDIN) . ' 00:00:00';

        $results = [['count', 'amount']];

        if ($withDocumentsCondition)
        {
            $results[] = $this->getStatistic($dateStart, $dateEnd, true);
        }

        if ($withOutDocumentsCondition)
        {
            $results[] = $this->getStatistic($dateStart, $dateEnd);
        }

        foreach ($results as $row) {
            printf("\n| %-6s| %-10s |", $row[0], $row[1]);
        }
    }

    /**
     * Получить статистику за период с документами(без документов)
     *
     * @param $dateStart
     * @param $dateEnd
     * @param bool $withDocuments
     * @return mixed
     */
    protected function  getStatistic($dateStart, $dateEnd, $withDocuments = false)
    {
        $query = 'SELECT count(p.id) as count, sum(p.amount) as amount ' .
                 'FROM payments p LEFT JOIN documents d ON (d.entity_id=p.id) ' .
                 'WHERE (p.create_ts BETWEEN :date_start AND :date_end) AND ';

        $documentCondition = $withDocuments ? 'NOT' : '';
        $q = $this->getDb()->prepare($query . 'd.id IS ' .$documentCondition. ' NULL');
        $q->execute([':date_start' => $dateStart,':date_end' => $dateEnd]);

        return $q->fetch(PDO::FETCH_NUM);
    }
}