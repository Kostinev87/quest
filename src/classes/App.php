<?php
/**
 * Created by PhpStorm.
 * User: Evgenii Kostin
 * Date: 17.02.16
 * Time: 1:55
 */


/**
 * Class App
 *
 * Обертка для исполнения методов утилиты
 */
class App {

    /** @var string Класс утилиты (можно будет придумать откуда его задавать) */
    private $class = 'Quest';
    /** @var array Список методов, разрешенных для исполнения */
    private static $classMethods = [];

    /** @var string Запрошенный метод */
    private $method;
    /** @var array Входные параметры */
    private $params;

    /**
     * @param $argc
     * @param $argv
     */
    public function __construct($argc, $argv)
    {
        $method = $argc > 1 ? $argv[1] : false;
        $this->setMethod($method)->checkMethod();

        $params = $argc > 2 ? array_slice($argv, 2, $argc-2) : false;
        $this->setParams($params)->run();
    }

    /**
     * Запуск команды с параметрами
     */
    public function run()
    {
        /** @var Quest $class */
        $class = $this->class;
        $methodFullName = $this->getMethodFullName();
        $class::instance()->$methodFullName($this->getParams());
    }

    /**
     * @param $method
     * @return $this
     */
    public function setMethod($method)
    {
        $this->method = mb_strtolower($method, 'utf-8');

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param $params
     * @return $this
     */
    public function setParams($params)
    {
        $this->params = $params;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Возвращает полное название метода
     * получает список разрешенных -> отдает полное название метода
     *
     * @return mixed
     */
    public function getMethodFullName()
    {
        return $this->getMethodsByClass($this->class)[$this->getMethod()];
    }

    /**
     * Проверяет наличие запроценного метода в классе,
     * в случае отсутствия выводит разрешенные для выполнения методы
     *
     * @return $this
     * @throws BadCommandException
     */
    private function checkMethod()
    {
        $methodsList = $this->getMethodsByClass($this->class);

        if (!($this->getMethod() && isset($methodsList[$this->getMethod()]))) {
            throw new BadCommandException('Accepted commands: ' . implode(', ', array_keys($methodsList)));
        }

        return $this;
    }

    /**
     * Возвращает список методов класса с префиксом "action*"
     *
     * @param $class
     * @return array
     */
    protected function getMethodsByClass($class)
    {
        if (!isset(self::$classMethods[$class]))
        {
            $methodsList = [];
            foreach (get_class_methods($class) as $method)
            {
                $methodKey =  mb_strtolower($method, 'utf-8');
                if (preg_match('/^action(.+?)$/iU', $methodKey))
                {
                    $methodKey = preg_replace('/^action(.+?)$/iU','$1', $methodKey);
                    $methodsList[$methodKey] = $method;
                }
            }
            self::$classMethods[$class] = $methodsList;
        }
        return self::$classMethods[$class];
    }
}
